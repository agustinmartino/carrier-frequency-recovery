import math
import numpy as np
import pylab as pl

def cordic_vectoring(input_x, input_y, iterations=32):

    #Defino caso corner para ser consistente con otras implementaciones
    if(input_x == 0 and input_y == 0):
        return 0,0
    
    #Cordic original funciona igual que arctg en el cuadrante 1 y 4
    # se extiende a los cuadrantes 2 y 3
    if(input_x < 0 and input_y<0):
        accum_x = -input_x
        accum_y = -input_y        
        accum_ang = -math.pi
    elif(input_x < 0 and input_y>=0):
        accum_x = -input_x
        accum_y = -input_y        
        accum_ang = math.pi        
    else:
        accum_x = input_x
        accum_y = input_y        
        accum_ang = 0

    #Arctg lut, guarda arctg(2^-i) para las N iteraciones
    # module_const, recupera el modulo original del vector luego de las
    # rotaciones
    atan_lut = []
    module_const = 1
    for kk in range(iterations):
        atan_lut.append(math.atan((2**-kk)))
        module_const = module_const * (1.0/math.sqrt(1+2**(-2*kk)))

    for it in range(iterations):
        # print("\n", accum_x)
        # print(accum_y)
        # print(accum_ang)
        # print(2**-it)
        if(accum_y > 0):
            new_accum_x = accum_x + 2**-it * accum_y
            new_accum_y = accum_y - 2**-it * accum_x
            accum_ang = accum_ang + atan_lut[it]
        else:
            new_accum_x = accum_x - 2**-it * accum_y
            new_accum_y = accum_y + 2**-it * accum_x
            accum_ang = accum_ang - atan_lut[it]
        accum_x = new_accum_x
        accum_y = new_accum_y

    return accum_x*module_const, accum_ang


def cart2pol (input_x, input_y):
    if(input_x < 0 and input_y<0):
        delta = -math.pi
    elif(input_x < 0 and input_y>0):
        delta = math.pi
    else:
        delta = 0
    if(input_x == 0):
        angle = math.pi/2
    else:
        angle = delta + math.atan(input_y/input_x)
    mod = math.sqrt(input_x**2+input_y**2)
    return mod, angle


def cordic_array_complex(samples, iterations):
    N = len(samples)
    module = np.zeros(N)
    angle = np.zeros(N)
    for kk in range(N):
        module[kk], angle[kk] = cordic_vectoring(samples[kk].real, samples[kk].imag, iterations)
    return module, angle


if __name__=='__main__':
    #-------------------------------------------------------
    # Analisis de casos corner y 4 cuadrantes
    #-------------------------------------------------------
    print("Caso 0.0")
    a = 0 + 0j
    print(np.angle(a))
    print (cart2pol(a.real,a.imag))
    print (cordic_vectoring(a.real,a.imag))
    print("1 cuadrant")
    a = 1 + 1j
    print(np.angle(a))
    print (cart2pol(1,1))
    print (cordic_vectoring(1,1))
    print("2 cuadrant")
    a = -1 + 1j
    print(np.angle(a))
    print (cart2pol(-1,1))
    print (cordic_vectoring(-1,1))
    print("3 cuadrant")
    a = -1 - 1j
    print(np.angle(a))
    print (cart2pol(-1,-1))
    print (cordic_vectoring(-1,-1))
    print("4 cuadrant")
    a = 1 - 1j
    print(np.angle(a))
    print (cart2pol(1,-1))
    print (cordic_vectoring(1,-1))


    #-------------------------------------------------------
    # Analisis de convergencia de angulo
    #-------------------------------------------------------
    a = 0.3 - 0.4j
    N = 16
    module = np.zeros(N)
    angle = np.zeros(N)
    for kk in range(N):
        module[kk], angle[kk] = cordic_vectoring(a.real, a.imag, kk)    
    pl.axhline(np.angle(a), label="Fase ideal", ls='--', color='r')
    pl.plot(angle, '.-', label="Fase en iteracion N")
    pl.xlabel("Iteracion")
    pl.ylabel("Fase [Rad]")
    pl.grid()
    pl.legend()
    pl.show()



    #-------------------------------------------------------
    # Analisis de performance vs numero de iteraciones
    #-------------------------------------------------------
    def random_test(L, MAX_AMP, CORDIC_ITERATIONS):
        samples = (-MAX_AMP/2 + MAX_AMP*np.random.rand(L)) + 1j*(-MAX_AMP/2+MAX_AMP**np.random.rand(L))
        print (samples)
        cordic_mod, cordic_angle = cordic_array_complex(samples, CORDIC_ITERATIONS)
        ideal_mod   = np.abs(samples)
        ideal_angle = np.angle(samples)
        error_mod   = ideal_mod - cordic_mod
        error_angle = ideal_angle - cordic_angle
        #Calcular SNR y error maximo
        snr_mod = 10*np.log10(np.mean(ideal_mod**2)/np.mean(error_mod**2))
        snr_angle = 10*np.log10(np.mean(ideal_angle**2)/np.mean(error_angle**2))
        maxerr_mod = max(abs(error_mod))
        maxerr_angle = max(abs(error_angle))
        return snr_mod, snr_angle, maxerr_mod, maxerr_angle

    L = 10000
    MAX_AMP = 4
    CORDIC_ITERATIONS = 9
    error_angle_list = []
    error_mod_list   = []
    snr_angle_list   = []
    snr_mod_list     = []
    iterations = np.arange(7,15)
    for it in iterations:
        snr_mod, snr_angle, maxerr_mod, maxerr_angle = random_test(L, MAX_AMP, it)
        error_angle_list.append(maxerr_angle)
        error_mod_list.append(maxerr_mod)
        snr_angle_list.append(snr_angle)
        snr_mod_list.append(snr_mod)

    pl.title("Error CORDIC vs numero de iteraciones")
    pl.plot(iterations, error_angle_list, '.-', label="Error max fase")
    pl.plot(iterations, error_mod_list, '.-', label="Error max mod")
    pl.ylabel("Error")
    pl.xlabel("Numero de iteraciones")
    pl.grid()
    pl.legend()
    pl.figure()

    pl.title("SNR CORDIC vs numero de iteraciones")
    pl.plot(iterations, snr_angle_list, '.-', label="SNR fase")
    pl.plot(iterations, snr_mod_list, '.-', label="SNR mod")
    pl.ylabel("SNR [dB]")
    pl.xlabel("Numero de iteraciones")
    pl.grid()
    pl.legend()
    pl.show()


    #-------------------------------------------------------
    # Analisis de distrubicion y limite de error
    #-------------------------------------------------------
    L = 10000
    MAX_AMP = 4
    CORDIC_ITERATIONS = 8
    samples = MAX_AMP*np.random.rand(L) + MAX_AMP*1j*np.random.rand(L)
    cordic_mod, cordic_angle = cordic_array_complex(samples, CORDIC_ITERATIONS)
    ideal_mod   = np.abs(samples)
    ideal_angle = np.angle(samples)
    error_mod   = ideal_mod - cordic_mod
    error_angle = ideal_angle - cordic_angle
    pl.title("Distribucion error de fase")
    pl.axvline(-math.atan(2**-(CORDIC_ITERATIONS-1)), ls=':', color='r', label='-arctg(2^-(N-1))')
    pl.axvline(math.atan(2**-(CORDIC_ITERATIONS-1)), ls='--', color='r', label='arctg(2^-(N-1))')
    pl.hist(error_angle, label="Distribucion de error")
    pl.grid()
    pl.legend()
    pl.show()
