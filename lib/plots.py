import pylab as pl
import numpy as np

"""
x: complex input symbols. Type: Array of shape Nx1 or 1xN
label: string to add to plot title
"""
def plot_constellation(x, label=''):
    pl.plot(pl.real(x),pl.imag(x), '.')
    pl.xlabel('Real Part')
    pl.ylabel('Imag Part')
    pl.grid(1)
    pl.title("Constellation at "+ label)
    pl.xlim([-4.5,4.5])
    pl.ylim([-4.5,4.5])
    pl.gca().set_aspect('equal')

def plot_cfr_metrics(phase_error, integrator, proportional_part, nco_input, BR, label=""):

    pl.subplot(211)
    pl.plot(phase_error,label=label)
    pl.grid(1)
    pl.xlabel('Samples')
    pl.ylabel('Phase Error [rad]')

    pl.subplot(212)
    pl.plot(integrator/np.pi*BR/2/1e6,label=label)
    pl.grid(1)
    pl.xlabel('Samples')
    pl.ylabel('PLL Integrator [MHz]')

    # pl.subplot(413)
    # pl.plot(proportional_part/np.pi*BR/2/1e6,label=label)
    # pl.grid(1)
    # pl.xlabel('Samples')
    # pl.ylabel('Prop. Part [MHz]')

    # pl.subplot(414)
    # pl.plot(nco_input/np.pi*BR/2/1e6,label=label)
    # pl.grid(1)
    # pl.xlabel('Samples')
    # pl.ylabel('NCO Input [MHz]')
    pl.legend(loc='best')

if __name__=='__main__':
    #plot_constellation([1+1j],label='Tx')
    pl.show()
