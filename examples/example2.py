"""
En este ejemplo mostramos un modelo simplificado del canal de comunicaciones asumiendo:
1. ISI totalmente compensada en el ecualizador
2. Error de clock totalmente compensado
3. El canal solo agrega ruido y offset de portadora

Luego se instancia un Carrier Frequency Recovery (CFR) ideal
4. Se cuenta BER y se mide MSE
5. Se muestran algunas metricas internas del PLL
"""

import sys
sys.path.append('./lib')
import pylab as pl
import numpy as np
from qam16_tx import Tx16QAM
from  channel import Channel
from cfr import CFR_Cartesian_Ideal_FLP_NoLatency
import plots
from ber_checker import BerChecker

# Parametros generales
BR=32e9 # Symbol rate
Lsim=100e3 # Longitud de simulacion
snr_db=17
lo_offset=10e6

kp = 1e-2
ki = kp/1000.

# Instanciacion de los modulos
tx=Tx16QAM() 
ch = Channel(snr_db,lo_offset,BR)
cfr_cart = CFR_Cartesian_Ideal_FLP_NoLatency(kp, ki)
bcheck = BerChecker()

# Ejecucion del modelo
tx_symbs = tx.execute(int(Lsim))
ch_out = ch.execute(tx_symbs)

# Se procesa la salida del canal con el FCR
cfr_out, phase_error,integrator,proportional_part,nco_input,nco_output =  cfr_cart.execute(ch_out)


# Recorto las senales para contar BER
cfr_in_cut = ch_out[int(Lsim/2):-1]
cfr_out_cut = cfr_out[int(Lsim/2):-1]
x_cut = tx_symbs[int(Lsim/2):-1]

# Ber check
mse_cfr_in, ser, ber_cfr_in = bcheck.execute(cfr_in_cut,x_cut)
mse_cfr_out, ser, ber_cfr_out = bcheck.execute(cfr_out_cut,x_cut)

print("")
print("Resultados para QAM16 @SNR={:2.2f}dB, LO={:2.2f}MHz:".format(snr_db, lo_offset))
print ("\t -> MSE: CFR IN={:2.2f}dB \t - \t CFR OUT={:2.2f}dB".format(10*np.log10(mse_cfr_in),10*np.log10(mse_cfr_out)))
print ("\t -> BER: CFR IN={:.2e} \t - \t CFR OUT={:.2e}".format(ber_cfr_in, ber_cfr_out))


pl.figure(figsize=(15,8))
pl.subplot(131)
plots.plot_constellation(tx_symbs, 'Tx out')
pl.subplot(132)
plots.plot_constellation(ch_out, 'Ch out')
pl.subplot(133)
plots.plot_constellation(cfr_out_cut, 'CFR Out')

pl.figure(figsize=(15,8))
plots.plot_cfr_metrics(phase_error, integrator, proportional_part, nco_input, BR, label="Ideal")

pl.show()


    
