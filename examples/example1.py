"""
En este ejemplo mostramos un modelo simplificado del canal de comunicaciones asumiendo:
1. ISI totalmente compensada en el ecualizador
2. Error de clock totalmente compensado
3. El canal solo agrega ruido y offset de portadora
4. Se cuenta BER y se mide MSE
"""
import sys
sys.path.append('./lib')
import pylab as pl
import numpy as np
from qam16_tx import Tx16QAM
from  channel import Channel
import plots
from ber_checker import BerChecker

# Parametros generales
BR=32e9 # Symbol rate
Lsim=100e3 # Longitud de simulacion
snr_db=170
lo_offset=0e6

# Instanciacion de los modulos
tx=Tx16QAM() 
ch = Channel(snr_db,lo_offset,BR)
bcheck = BerChecker()

# Ejecucion del modelo
tx_symbs = tx.execute(int(Lsim))
ch_out = ch.execute(tx_symbs)

# Recorto las senales para contar BER
y_cut = ch_out[int(Lsim/2):-1]
x_cut = tx_symbs[int(Lsim/2):-1]

# Ber check
mse, ser, ber = bcheck.execute(y_cut,x_cut)

print("")
print("Resultados para QAM16 @SNR={:2.2f}dB, LO={:2.2f}MHz:".format(snr_db, lo_offset))
print ("\t -> MSE: {:2.2f}dB".format(10*np.log10(mse)))
print ("\t -> BER: {:.2e}".format(ber))


pl.figure(figsize=(15,8))
pl.subplot(121)
plots.plot_constellation(tx_symbs, 'Tx out')
pl.subplot(122)
plots.plot_constellation(ch_out, 'Ch out')
pl.show()


    
